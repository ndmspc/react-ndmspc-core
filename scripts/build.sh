#!/bin/bash
set -e

export CI=${CI-true}
PROJECT_DIR="$(dirname $(dirname $(readlink -m $0)))"
cd $PROJECT_DIR
$PROJECT_DIR/scripts/reset.sh
npm install
# npx npm-install-peers
npm run predeploy
cd $PROJECT_DIR