import axios from 'axios'

const simpleHttpGet = (store) => {
  console.log('The link was clicked.')
  axios.get(`https://httpbin.org/get`).then((res) => {
    const data = res.data
    store.setData({ data })
  })
}

export { simpleHttpGet }
