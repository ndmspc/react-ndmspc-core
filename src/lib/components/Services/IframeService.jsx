import { useEffect, useState } from 'react';
import { useNdmSpcLocal } from '../../'

export const IframeService = () => {

  const [ndmspcAnalyses, ndmspcAnalysesStore] = useNdmSpcLocal("ndmspcAnalyses");
  const [currentAnalysis, setCurrentAnalysis] = useState(null)
  const handlePostMessage = (event) => {
    // console.log("Event: ", event)
    
    if (event?.data?.action === "load") {
      // console.log(event.data)
      const ndmspcConfigString = event.data.content;
      const ndmspcConfig = JSON.parse(ndmspcConfigString)
      console.log("Loading configuration from iframe parent : ", ndmspcConfig)

      setCurrentAnalysis(ndmspcConfig)

    }
    else if (event?.data?.action === "init_save") {
      console.log("save:", JSON.stringify(ndmspcAnalyses))
      window.parent.postMessage({ event: "save", content: JSON.stringify(ndmspcAnalyses) }, '*');
    }
  };

  useEffect(() => {
    window.parent.postMessage({ event: "init" }, '*');
    window.addEventListener('message', handlePostMessage);
    () => {
      window.removeEventListener('message', handlePostMessage);
    };
  }, []);

  useEffect(() => {
    if (!currentAnalysis) return
    if (!ndmspcAnalyses) return

    console.log(JSON.stringify(ndmspcAnalyses).length)
    console.log(JSON.stringify(currentAnalysis).length)
    console.log(JSON.stringify(ndmspcAnalyses) === JSON.stringify(currentAnalysis))

    if (JSON.stringify(ndmspcAnalyses) !== JSON.stringify(currentAnalysis)) {
      if (ndmspcAnalyses.analysis) {
        if (ndmspcAnalyses.analysis.length > 0) {
          console.log("save:", JSON.stringify(ndmspcAnalyses))
          window.parent.postMessage({ event: "upload", content: ndmspcAnalyses }, '*')
        }
        setCurrentAnalysis(ndmspcAnalyses)
      } else {
        ndmspcAnalysesStore.setData(currentAnalysis)
      }
    }
  }, [currentAnalysis, ndmspcAnalyses]);


  return null;
};
