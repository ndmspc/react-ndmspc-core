import { useState, useEffect, useContext } from 'react';
import { useStreamBrokerIn, useStreamBrokerOut, NdmSpcContext, useNdmSpcLocal, useNdmSpcRedirect, zmq2wsUrlToExecutor } from "../../";

export const WebSocketService = ({ name = "zmq2ws" }) => {
  const sb = useContext(NdmSpcContext)[name];
  const [url, setUrl] = useState("");
  const [connected, setConnected] = useState(false);
  const [loading, setLoading] = useState(false);
  const _ws = useStreamBrokerOut("_ws", name);
  const app = useStreamBrokerIn("app", name);
  // const [ndmspcApp, ndmspcAppStore] = useNdmSpcLocal("ndmspcApp");
  const [ndmspcZmq2ws, ndmspcZmq2wsStore] = useNdmSpcLocal("ndmspcZmq2ws");

  const [ndmspcInternal, ndmspcInternalStore] = useNdmSpcRedirect("ndmspcInternal");
  const [, ndmspcClusterSubscribeStore] = useNdmSpcRedirect("ndmspcClusterSubscribe");

  useEffect(() => {
    if (Object.keys(_ws).length) {
      console.log(_ws);
      if (_ws.type === "_ws") {
        if (_ws.action === "connect") {
          setUrl(_ws.payload.url);
        } else if (_ws.action === "disconnect") {
          setConnected(false);
          setLoading(false);
          setUrl("")
          ndmspcClusterSubscribeStore.setData({ action: "subscribe-clear" })
          ndmspcZmq2wsStore.setData({ ...ndmspcZmq2ws, url: null, submitUrl: null, executorUrl: null, connected: false, loading: false, app: null, subscribed: false });
        } else if (_ws.action === "state") {
          // console.log(_ws.payload)
          setConnected(_ws.payload.connected);
          setLoading(_ws.payload.loading);
          if (!_ws.payload.connected) {
            setUrl("")
            ndmspcClusterSubscribeStore.setData({ action: "subscribe-clear" })
          }
          const d = { ...ndmspcZmq2ws, url: _ws.payload.url, executorUrl: _ws.payload.connected ? zmq2wsUrlToExecutor(_ws.payload.url) : null, connected: _ws.payload.connected, loading: _ws.payload.loading, subscribed: _ws.payload.connected ? ndmspcZmq2ws.subscribed : false }
          // console.log(d)
          ndmspcZmq2wsStore.setData(d);

        }
      }
    }
  }, [_ws]);

  useEffect(() => {

    const executorUrl = zmq2wsUrlToExecutor(url)
    const appinfo = connected ? app : null;
    const msg = { type: "api", action: "state", name, url, executorUrl, connected, loading, app: appinfo };
    // console.log(msg);
    ndmspcInternalStore.setData(msg);

    // console.log(apiurl)
    const zmq2wsMsg = { ...ndmspcZmq2ws, url, connected, loading, app: appinfo, executorUrl, subscribed: connected ? ndmspcZmq2ws.subscribed : false }
    // console.log(zmq2wsMsg);
    ndmspcZmq2wsStore.setData(zmq2wsMsg);

  }, [url, loading, connected, app]);

  useEffect(() => {
    if (!ndmspcInternal) return
    if (ndmspcInternal.type === "ws") {
      if (ndmspcInternal.action === "connect") {
        sb && sb.connect(ndmspcInternal.url);
      } else if (ndmspcInternal.action === "disconnect") {
        sb && sb.disconnect();
        setConnected(false);
        setLoading(false);
      }
    }
  }, [sb, ndmspcInternal]);
  // useEffect(() => {
  //   console.log("WebSocketService rerender");
  //   // ndmspcInternalStore.setData({ type: "ui", action: "resize" });
  // });

  return null;
};
