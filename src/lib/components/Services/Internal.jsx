import React from "react";
import { WindowResizeService } from './WindowResizeService';
import { WebSocketService } from './WebSocketService';
import { Zmq2wsSubscriptionService } from './Zmq2wsSubscriptionService';
import { PluginService } from './PluginService';
import { IframeService } from "./IframeService";

export const NdmspcInternal = () => {
  return (
    <>
      <IframeService />
      <WindowResizeService />
      <WebSocketService />
      <Zmq2wsSubscriptionService />
      <PluginService />
    </>
  );
};