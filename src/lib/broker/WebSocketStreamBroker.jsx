import { StreamBroker } from './StreamBroker'

export const WebSocketStreamBroker = (sb, url = '') => {
  if (sb === undefined) sb = new StreamBroker(1)
  let ws = undefined
  let internalSub = undefined

  if (url === '') {
    sb.publishState(false, false, url)
    internalSub = sb.subscribeOut((data) => {
      if (data.type === '_ws') {
        if (data.action === 'connect') {
          WebSocketStreamBroker(sb, data.payload.url)
        }
      }
    })
  } else {
    sb.publishState(true, false, url)
    ws = new WebSocket(url)
    ws.onopen = () => {
      console.log(`[@ndmspc/Websock] - Connected to ${ws.url}`)
      sb.publishState(false, true, ws.url)
      internalSub = sb.subscribeOut((data) => {
        if (data.type === '_ws') {
          if (data.action === 'connect') {
            ws.close()
            WebSocketStreamBroker(sb, data.payload.url)
          } else if (data.action === 'disconnect') {
            ws.close()
            ws == undefined
          }
        } else {
          ws.send(JSON.stringify(data))
        }
      })
    }
    ws.onmessage = (event) => {
      const msg = JSON.parse(event.data)
      sb.onmessage(msg)
    }
    ws.onclose = (event) => {
      if (event.originalTarget) {
        console.log(
          `[@ndmspc/Websock] - Disconected from ${event.originalTarget.url}`
        )
      }
      sb.publishState(false, false, event.originalTarget.url)
      if (internalSub !== undefined) internalSub.unsubscribe()
    }
  }
  return sb
}
