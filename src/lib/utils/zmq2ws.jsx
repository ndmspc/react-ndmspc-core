
export const getKeyValueFromZmq2wsServicesArray = (arr) => {
  let temp = arr.reduce((prev, curr) => {
    //servicesTree array transform to treelike form
    if (!prev[curr.name]) {
      prev[curr.name] = {
        name: curr.name,
        src: curr.src,
        subs: [curr.sub],
      };
    } else {
      prev[curr.name].subs.push(curr.sub);
    }
    return prev;
  }, {});
  return temp;
}


export const transformSubmitUrl = (submitUrl) => {
  let url = submitUrl
  if (url.startsWith("tcp://ndmspc-sample")) url = "tcp://ndmspc-sample-sls-submitter-service:41000";
  if (url.startsWith("tcp://eos0.ndmspc.io")) url = "tcp://147.213.204.139:41000";
  return url
}

export const zmq2wsUrlToExecutor = (zmq2wsUrl) => {
  if (!zmq2wsUrl) return null
  let url = zmq2wsUrl
  let isWsSecure = false
  if (url.startsWith("wss://")) {
    isWsSecure = true
    url = url.replace("wss://", "")
  } else {
    url = url.replace("ws://", "")
  }

  if (url.startsWith("zmq2ws")) {
    url = `${isWsSecure ? "https://" : "http://"}${url.replace("zmq2ws", "executor")}/api/v1`
  } else {
    url = `${isWsSecure ? "https://" : "http://"}${url.replace("zmq2ws", "")}api/v1`
  }

  return url
}
