export const isObjectEmpty = (objectName) => {
  return (
    objectName &&
    Object.keys(objectName).length === 0 &&
    objectName.constructor === Object
  );
};

export const getRandomInt = (max = 10) => {
  return Math.floor(Math.random() * max);
}



export async function urlExists(url) {
  const result = await fetch(url, { method: "HEAD", cache: "reload" });
  // console.log(result)
  return result.ok;
}

export const http2root = (url) => {
  const httpUrlObj = new URL(url);
  return [`root://${httpUrlObj.host}/`, httpUrlObj.pathname, `root://${httpUrlObj.host}/${httpUrlObj.pathname}`];
}

export const root2http = (url, postfix = "s") => {
  let rootUrl = url.replace("root://", "").replace(/\/\//g, "/");
  rootUrl = `http${postfix}://${rootUrl}`;
  const httpUrlObj = new URL(rootUrl);
  // console.log(url, rootUrl);
  return [`http${postfix}://${httpUrlObj.host}`, httpUrlObj.pathname, `http${postfix}://${httpUrlObj.host}${httpUrlObj.pathname}`];
}


export const httpRequest = (url, body, method = "POST") => {
  // console.log(url);
  // console.log(JSON.stringify(body));

  return fetch(url, {
    method,
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  }).then((res) => {
    if (res.ok) {
      return res.json();
    } else {
      return Promise.reject(res.error);
    }
  });
};

