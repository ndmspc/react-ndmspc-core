/* global localStorage */
import { ReplaySubject } from 'rxjs'

const initialState = {}

export const localStore = (
  key,
  state = initialState,
  subject = new ReplaySubject(1)
) => ({
  init: (_key = key) => {
    const data = localStorage.getItem(_key)
    if (data === null) {
      state = { ...state, name: _key, data: {} }
      localStorage.setItem(_key, JSON.stringify(state.data))
    } else {
      state = { ...state, name: _key, data: JSON.parse(data) }
    }
    subject.next(state)
  },
  subscribe: (setState) => subject.subscribe(setState),
  setData: (data) => {
    state = {
      ...state,
      data
    }
    localStorage.setItem(key, JSON.stringify(state.data))
    subject.next(state)
  },
  save: () => {
    localStorage.setItem(key, JSON.stringify(state.data))
    subject.next(state)
  },
  clear: () => {
    state = {}
    localStorage.setItem(key, JSON.stringify(state.data))
    subject.next(state)
  },
  clearLocalStorage: () => {
    localStorage.removeItem(key)
  },
  initialState
})
