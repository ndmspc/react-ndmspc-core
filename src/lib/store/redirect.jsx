import { ReplaySubject } from 'rxjs'

const initialState = {}
export const redirectStore = (
  cache = 1,
  subject = new ReplaySubject(cache)
) => ({
  init: () => {},
  subscribe: (setState) => subject.subscribe(setState),
  setData: (data) => {
    subject.next(data)
  },
  clear: () => {
    subject.next({})
  },
  get subject() {
    return subject
  },
  initialState
})
