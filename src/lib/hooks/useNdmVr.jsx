import { useContext } from 'react';
import { NdmVrContext, useRedirectStore, useLocalStore, isObjectEmpty } from "../index";

export const useNdmVrLocal = (name = "ndmspcApp") => {
  const store = useContext(NdmVrContext)[name]
  const local = useLocalStore(store)
  return [local?.data, store]
}

export const useNdmVrRedirect = (name = "ndmspcApp") => {
  const store = useContext(NdmVrContext)[name]
  const local = useRedirectStore(store)
  if (isObjectEmpty(local)) return [null, store]
  return [local, store]
}
