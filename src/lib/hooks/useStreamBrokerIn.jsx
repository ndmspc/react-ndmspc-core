import { useContext, useLayoutEffect, useState } from 'react'
import NdmSpcContext from '../contexts/NdmSpcContext'

export default (type = '', wssb = 'zmq2ws') => {
  const [local, setLocal] = useState({})
  const sb = useContext(NdmSpcContext)[wssb]
  useLayoutEffect(() => {
    if (sb === null) return
    const subs = sb.subscribeIn(setLocal, type)
    return () => subs.unsubscribe()
  }, [sb])

  return local
}
