import { useLayoutEffect, useState } from 'react'

export default (store, functions) => {
  const [result, setResult] = useState(0)

  const handleState = (data) => {
    setResult(functions[data.index](data.args))
  }

  useLayoutEffect(() => {
    const subs = store.subscribe(handleState)
    return () => subs.unsubscribe()
  }, [store, functions])

  const executeFunction = () => {
    return result
  }

  return executeFunction()
}
