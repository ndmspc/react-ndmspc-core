import { localStore } from './store/local'
import { redirectStore } from './store/redirect'
import { executorStore } from './store/executor'
import useLocalStore from './hooks/useLocalStore'
import useRedirectStore from './hooks/useRedirectStore'
import useExecutorStore from './hooks/useExecutorStore'
import useStreamBrokerIn from './hooks/useStreamBrokerIn'
import useStreamBrokerOut from './hooks/useStreamBrokerOut'
import StorageService from './utils/storageService'
import HistogramStorageService from './utils/histogramStorageService'

import {
  createHistogram,
  openFile,
  version,
  createTGraph,
  create,
  draw,
  redraw,
  createTMultiGraph,
  createTPolyLine,
  createTHStack,
  createHttpRequest
} from 'jsroot'

import NdmSpcContext from './contexts/NdmSpcContext'
import NdmVrContext from './contexts/NdmVrContext'

import {
  createTH1Projection,
  openTH1Projection,
  displayImageOfProjection,
  readProjectionFromFile,
  appendContextMenu,
  createTH2DemoHistogram,
  createTH3DemoHistogram
} from './jsroot/tools'

import {
  isObjectEmpty,
  getRandomInt,
  urlExists,
  http2root,
  root2http,
  httpRequest
} from './utils/common'

import {
  getBinId,
  readJsrootObjectFromFile,
  getFileObjectNames,
  drawJsrootObjectsFromFile
} from './utils/histogram'


import {
  getKeyValueFromZmq2wsServicesArray,
  transformSubmitUrl,
  zmq2wsUrlToExecutor
} from './utils/zmq2ws'

import {
  submitSalsaJob
} from './utils/salsa'

import useAuth from './hooks/useAuth'

import {
  useNdmSpcLocal,
  useNdmSpcRedirect,
  useNdmSpcJobMontitor,
  useNdmSpcPlugins,
  useNdmSpcPlugin
} from './hooks/useNdmSpc'

import {
  useNdmVrLocal,
  useNdmVrRedirect,
} from './hooks/useNdmVr'

import { IframeService } from './components/Services/IframeService'
import { PluginService } from './components/Services/PluginService'
import { WebSocketService } from './components/Services/WebSocketService'
import { WindowResizeService } from './components/Services/WindowResizeService'
import { Zmq2wsSubscriptionService } from './components/Services/Zmq2wsSubscriptionService'
import { NdmspcInternal } from './components/Services/Internal'




import AppGlobalScope from './globals/AppGlobalScope'
import Distributor from './globals/Distributor'
import { StreamBroker } from './broker/StreamBroker'
import { WebSocketStreamBroker } from './broker/WebSocketStreamBroker'

export {
  useAuth,
  localStore,
  redirectStore,
  executorStore,
  useLocalStore,
  useRedirectStore,
  useExecutorStore,
  useStreamBrokerIn,
  useStreamBrokerOut,
  NdmSpcContext,
  useNdmSpcLocal,
  useNdmSpcRedirect,
  useNdmSpcJobMontitor,
  useNdmSpcPlugins,
  useNdmSpcPlugin,
  NdmVrContext,
  useNdmVrLocal,
  useNdmVrRedirect,
  StorageService,
  HistogramStorageService,
  createHistogram,
  openFile,
  version,
  createTGraph,
  create,
  draw,
  redraw,
  createTMultiGraph,
  createTPolyLine,
  createTHStack,
  createHttpRequest,
  createTH1Projection,
  openTH1Projection,
  displayImageOfProjection,
  readProjectionFromFile,
  appendContextMenu,
  createTH2DemoHistogram,
  createTH3DemoHistogram,
  isObjectEmpty,
  getRandomInt,
  urlExists,
  http2root,
  root2http,
  httpRequest,
  getBinId,
  readJsrootObjectFromFile,
  getFileObjectNames,
  drawJsrootObjectsFromFile,
  submitSalsaJob,
  getKeyValueFromZmq2wsServicesArray,
  transformSubmitUrl,
  zmq2wsUrlToExecutor,
  AppGlobalScope,
  Distributor,
  StreamBroker,
  WebSocketStreamBroker,
  NdmspcInternal,
  IframeService,
  PluginService,
  WebSocketService,
  WindowResizeService,
  Zmq2wsSubscriptionService
}
