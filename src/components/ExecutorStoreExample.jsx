import React, { useEffect } from 'react'

import { useExecutorStore } from '../lib'

const functions = ['first', 'second', 'third', 'fourth', 'fifth'].map((option, index) => (data) => {
    console.log(`I am ${index}"+ ${data}`)
    return `I am ${index}"+ ${data}`
})

const ExecutorInfoExample = ({ store }) => {
    const state = useExecutorStore(store, functions)
    const handlers = ["AAAA", "BBBB", "CCCC", "DDDD", "EEEE"].map((option, index) => () => {
        store.executeFunction(index, option)
    })
    const showBin1 = () => {
        console.log(store.getSelectedBin())
    }
    const storeBin1 = () => {
        store.setSelectedBin("{bin info 1 }")
    }
    const storeBin2 = () => {
        store.setSelectedBin("{bin info 2 }")
    }

    useEffect(() => {
        store.setOptions(['first', 'second', 'third', 'fourth', 'fifth'])
  }, [store])

    return (
        <>
            <h2>{`Returned value: ${state}`}</h2>
            <div>
                <button onClick={handlers[0]}>execute first</button>
                <button onClick={handlers[1]}>execute secound</button>
                <button onClick={handlers[2]}>execute third</button>
                <button onClick={handlers[3]}>execute fourth</button>
                <button onClick={handlers[4]}>execute fifth</button>
            </div>
            <div>
                <button onClick={storeBin1}>Store bin 1</button>
                <button onClick={storeBin2}>Store bin 2</button>
            </div>
            <div>
                <button onClick={showBin1}>show bin</button>
            </div>
        </>
    )
}

export default ExecutorInfoExample