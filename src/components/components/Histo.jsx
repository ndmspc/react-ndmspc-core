import React, { useEffect } from 'react'
import { openFile, redraw } from 'jsroot'
import { appendContextMenu} from '../../lib'
import { broker } from '../../AppGlobalContext'

const Histo = ({ store, filename = 'https://eos.ndmspc.io/eos/ndmspc/scratch/test/test-results-final-out.root' }) => {

  const viewDistributor = store.viewDistributor
  const projNames = ['hSignal', 'hSignalBg', 'hBgNorm', 'hSignalFit']
  const operations = ['click', 'dbClick', 'hover']

  // extending the function
  const extendedOnBinClick = (data) => {
    data.bannerIds = ['proj1', 'proj2', 'proj2', 'proj2']
    data.projNames = projNames
    console.log('click')
    store.userFunctions.onClick(data)
  }

  const extendedOnBinDbClick = (data) => {
    data.bannerIds = ['proj1', 'proj2', 'proj2', 'proj2']
    console.log('DbClick')
    data.projNames = projNames
    store.userFunctions.onDbClick(data)
  }

  const extendedOnBinHover = (data) => {
    if (data) {
      data.bannerIds = ['proj1', 'proj2', 'proj3', 'proj4']
      console.log('hover')
      data.projNames = projNames
      store.userFunctions.onHover(data)
    }
  }

  const extendedOnContextOption = (data) => {
    if (data) {
      viewDistributor.sendOutputData({ data: data, event: 'CONTEXT' })
    }
  }

  const drawHistogram = async () => {
    const file = await openFile(filename)
    const hist = await file.readObject('pt_vs_mult/hist')
    const painter = await redraw('proj1', hist, 'colz')
    if (painter != null) {
      await appendContextMenu(painter, projNames, operations, extendedOnContextOption)
      painter.configureUserClickHandler(extendedOnBinClick)
      painter.configureUserTooltipHandler(extendedOnBinHover, 1000)
      painter.configureUserDblclickHandler(extendedOnBinDbClick)
    }
  }

  useEffect(() => {
    const subscription = broker.createExternalSubscription(
      'view',
      (toUpdateComponent) => {
        console.log(toUpdateComponent)
        console.log('updatujem komponent JsrootHistogram')
      }
    )
    return () => subscription.unsubscribe()
  }, [])

  useEffect(() => {
    drawHistogram()
  }, [filename])


  const projectionStyle = {
    width: '400px',
    height: '400px',
    background: 'grey',
    marginLeft: '1rem'
  }

  return (
    <div id='proj1' style={projectionStyle}></div>
  )
}

export default Histo
