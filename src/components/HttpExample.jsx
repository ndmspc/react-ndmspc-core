import React from 'react'
import { JsonViewer } from '@textea/json-viewer'
import { useRedirectStore } from '../lib'
import { simpleHttpGet } from '../utils/http'

const HttpExample = ({ store }) => {
  const state = useRedirectStore(store.http)

  const handleHttp = () => {
    simpleHttpGet(store.http)
  }

  const handleClear = () => {
    store.http.setData({})
  }

  return (
    <>
      <button onClick={handleHttp}>Refresh</button>
      <button onClick={handleClear}>Clear</button>
      <JsonViewer value={state} />
    </>
  )
}

export default HttpExample
