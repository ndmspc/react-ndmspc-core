import React from 'react'
import { JsonViewer } from '@textea/json-viewer'
import { useRedirectStore } from '../lib'

const RedirectExample = ({ store }) => {
  const state = useRedirectStore(store)

  return <JsonViewer value={state}/>
}

export default RedirectExample
