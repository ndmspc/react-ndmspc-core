import React, { useState } from 'react'
import Configurator from './components/Configurator'
import Histo from './components/Histo'
import Projection from './components/Projection'
import { broker } from '../AppGlobalContext'

const DistributorExample = ({ store }) => {

  const [handlerState, setHandlerState] = useState(false)

  const changeState = () => {
    if (handlerState) {
      broker.clearAllSubscriptions()
    } else {
      broker.addAllSubscriptions()
    }
    setHandlerState(!handlerState)
  }

  const style = {
    display: 'flex',
    flexDirection: 'row',
    margin: '1rem',
    padding: '1rem',
    border: '2px solid black'
  }

  return <div>
    <button onClick={changeState}>{handlerState ? 'disable' : 'enable'}</button>
    <div style={style}>
      <Configurator store={store} />
      <div style={{ display: 'flex', flexDirection: 'row-reverse', width: '80%' }}>
        <Histo store={store} filename={'https://eos.ndmspc.io/eos/ndmspc/scratch/out.root'}/>
        <Projection filename={'https://eos.ndmspc.io/eos/ndmspc/scratch/out.root'}/>
      </div>
    </div>
  </div>
}

export default DistributorExample
