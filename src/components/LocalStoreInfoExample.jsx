import React, { useEffect } from 'react'
import { JsonViewer } from '@textea/json-viewer'
import { useLocalStore } from '../lib'

const LocalStoreInfoExample = ({ store, delay = 1000 }) => {
  const state = useLocalStore(store)
  useEffect(() => {
    const interval = setInterval(() => {
      store.setData({ random: parseInt(delay * Math.random()) })
    }, delay)
    return () => clearInterval(interval)
  }, [store, delay])

  return <JsonViewer value={state.data} />
}

export default LocalStoreInfoExample
