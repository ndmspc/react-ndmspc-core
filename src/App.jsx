import React from 'react'
import { version } from 'jsroot'
import {
  NdmSpcContext,
  WebSocketStreamBroker
} from './lib'
import LocalStoreInfoExample from './components/LocalStoreInfoExample'
import RedirectExample from './components/RedirectExample'
import HttpExample from './components/HttpExample'
import ExecutorInfoExample from './components/ExecutorStoreExample'
import DistributorExample from './components/DistributorExample'
import HistogramFunctionsExample from './components/HistogramFunctionsExample'
import StreamBrokerExample from './components/StreamBrokerExample'
import WebSocketConnectForm from './components/WebSocketConnectForm'
import AuthExample from './components/AuthExample'
import { store } from './AppGlobalContext'


const App = () => {
  return (
    <>
      {version && <h2>JSROOT Version: {version}</h2>}
      <NdmSpcContext.Provider
        value={{
          zmq2ws: WebSocketStreamBroker(),
          shared: WebSocketStreamBroker()
        }}
      >
        <WebSocketConnectForm store={store.zmq2wsUrl} wssb='zmq2ws' />
        <StreamBrokerExample wssb='zmq2ws' />
        <WebSocketConnectForm store={store.sharedUrl} wssb='shared' />
        <StreamBrokerExample wssb='shared' />
      </NdmSpcContext.Provider>
      <AuthExample />

      <LocalStoreInfoExample store={store.local} delay='1000' />
      <LocalStoreInfoExample store={store.localSecond} delay='1500' />
      <ExecutorInfoExample store={store.executor} />
      <RedirectExample store={store.redirect} />
      <HttpExample store={store} />
      <DistributorExample store={store} />
      <HistogramFunctionsExample />
    </>
  )
}

export default App
